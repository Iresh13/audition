import React from "react";
import {
 StyleSheet,
 TextInput,
 Text,
 View,SafeAreaView,
 ScrollView,Dimensions,
 TouchableOpacity
} from "react-native";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import Icon from "react-native-vector-icons/AntDesign";
import Button from "../components/button";
import AppSettings from "../utils/settings";
import Toolbar from "../components/toolbar";
class Register extends React.Component {
 constructor() {
   super();
   this.state = {
     name: "",
     Phone: "",
     Address: "",
     Gender: "",
     Email: ""
   };
 }
 static get options() {
   return {
     topBar: {
       visible: false,
       drawBehind: true
     }
   };
 }
 render() {
   return (
     <SafeAreaView style={[styles.main,{padding:0}]}>
    <Toolbar title={"Register Now"} componentId={this.props.componentId}/>
     <KeyboardAwareScrollView style={styles.main}>
       <View style={styles.Top}>
         <Text style={styles.header}>Be a Part With Us</Text>
       </View>
       <View style={styles.form}>
         <View style={styles.box}>
           <Icon name={"user"} size={20} color={"black"} />
           <TextInput
             placeholder="Name"
             onChangeText={text => this.setState({ name: text })}
             style={styles.textinput}
           />
         </View>
         <View style={styles.box}>
           <Icon name={"home"} size={20} color={"black"} />
           <TextInput
             placeholder="Address"
             onChangeText={text => this.setState({ Address: text })}
             style={styles.textinput}
           />
         </View>
         <View style={styles.box}>
           <Icon name={"phone"} size={20} color={"black"} />
           <TextInput
             placeholder="Phone"
             onChangeText={text => this.setState({ Phone: text })}
             style={styles.textinput}
           />
         </View>
         <View style={styles.box}>
           <Icon name={"team"} size={20} color={"black"} />
           <TextInput
             placeholder="Gender"
             onChangeText={text => this.setState({ Gender: text })}
             style={styles.textinput}
           />
         </View>
         <View style={styles.box}>
           <Icon name={"mail"} size={20} color={"black"} />
           <TextInput
             placeholder="Email"
             onChangeText={text => this.setState({ Email: text })}
             style={styles.textinput}
           />
         </View>
       </View>
    
     </KeyboardAwareScrollView>
        <TouchableOpacity style={styles.button}>
        <Icon name={"upload"} size={20} color={"black"} />
        <Text style={styles.font}>Choose Video</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.bottombutton}>
     <Button title={"Submit"} backgroundColor={AppSettings.secondaryColor} borderColor={AppSettings.secondaryColor}/>
      </TouchableOpacity>
      </SafeAreaView>
   );
 }
}
const {width,height} = Dimensions.get("screen")
const styles = StyleSheet.create({
 main: {
   flex: 1,
   backgroundColor: AppSettings.primaryColor,
padding:30
 },
 header:{
   fontFamily:AppSettings.primaryfont,
   fontSize:AppSettings.secondaryFontSize,
   fontStyle:"italic",
   fontWeight:"400",paddingVertical:20,color:AppSettings.white
 },
 box: {
   height: 50,
   alignItems: "center",
   backgroundColor: "white",
   borderRadius: 10,
   flexDirection: "row",
   marginVertical: 10,
   paddingHorizontal: 10
 },
 button: {
   height: 50,
   alignItems: "center",
   backgroundColor: "white",
   borderRadius: 10,
   flexDirection: "row",
   marginVertical: 10,
   paddingHorizontal: 10,
   position: "absolute",
 bottom:100,
   right: 20
 },bottombutton:{
   flex:1,position:"absolute",bottom:40,right:20,left:20
 },textinput:{
   color:AppSettings.black,fontFamily:AppSettings.primaryfont,
   paddingLeft:20
 },font:{
  fontFamily:AppSettings.primaryfont,
  fontSize:AppSettings.ternaryFontSize,
paddingLeft:10,
  fontWeight:"400",color:AppSettings.black
 },Top:{
   flex:1,marginTop:-20
 }
});
export default Register;