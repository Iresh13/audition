import React from "react";
import {
 View,
 TouchableOpacity,
 StyleSheet,
 Text,
 FlatList,
 Image,
 AsyncStorage,
 ScrollView,
 SafeAreaView,
 Platform
} from "react-native";
import Icon from "react-native-vector-icons/AntDesign";
import { Navigation } from "react-native-navigation";
import AppSettings from "../utils/settings";
import FastImage from "react-native-fast-image";
class MenuDrawer extends React.Component {
 constructor() {
   super();
   this.state = {
     name: "Hello",
     image:require("../assets/user.png"),
     menus: [
       { title: "About Us", name: "infocirlceo", component: "about" },
       { title: "Sponsers", name: "solution1", component: "sponser" },
       { title: "Judges", name: "team", component: "judges" },
       { title: "Nearby", name: "enviromento", component: "nearby" },
       { title: "Feedback", name: "like2", component: "feedback" },
     ]
   };
 }
 static get options() {
   return {
     topBar: {
       visible: false,
       drawBehind: false
     }
   };
 }
 goTo = name => {
   Navigation.push("App", {
     component: {
       name: name
     }
   }),
     Navigation.mergeOptions("Drawer", {
       sideMenu: {
         left: {
           visible: false,
           enabled: false
         }
       }
     });
 };
 Logout=async()=>{
    Navigation.push("App", {
        component: {
          name: "login"
        }
      }),
        Navigation.mergeOptions("Drawer", {
          sideMenu: {
            left: {
              visible: false,
              enabled: false
            }
          }
        });
 }
 render() {
   return (
     <View style={styles.main}>
       <View style={styles.top}>
         <View style={styles.circle}>
          <FastImage source={this.state.image} style={styles.images}/>
         </View>
         <Text style={styles.name}>{this.state.name}</Text>
       </View>
       <FlatList
         showsHorizontalScrollIndicator={false}
         showsVerticalScrollIndicator={false}
         data={this.state.menus}
         renderItem={({ item }) => {
           return (
             <TouchableOpacity
               style={styles.menu}
              onPress={()=>this.goTo(item.component)}
             >
               <Icon name={item.name} size={20} color={"#f8faf8"} />
               <Text style={styles.menutext}>{item.title}</Text>
             </TouchableOpacity>
           );
         }}
         keyExtractor={item => item.title}
       />
       <TouchableOpacity style={styles.Logout} onPress={()=>this.Logout()}>
         <Icon name={"logout"} size={20} color={"#f8faf8"} />
         <Text style={styles.menutext}>Logout</Text>
       </TouchableOpacity>
     </View>
   );
 }
}
const styles = StyleSheet.create({
 main: {
   backgroundColor: AppSettings.primaryColor,
   flex: 1,paddingBottom:30
 },
 top: {
   backgroundColor: AppSettings.secondaryColor,
   height: 250,
   justifyContent: "center",
   alignItems: "center",
   elevation: 5,
   shadowOpacity: 0.2,
   shadowColor: "#000000",padding:40},
 menu: {
   alignItems: "center",
   flex: 1,
   borderBottomColor: "#f8faf8",
   borderBottomWidth: 0.2,
   flexDirection: "row",
   paddingVertical: 20,
   paddingHorizontal: 20
 },
 circle: {
   height: 100,
   width: 100,
   borderRadius: 50,
   backgroundColor: AppSettings.white,
   justifyContent: "center",
   alignItems: "center"
 },
 images:{
    height: 98,
    width: 98,
    borderRadius: 49,  
 },
 Logout: {
   position: "absolute",
   bottom: 2,
   right: 0,
   left: 0,
   alignItems: "center",
   flex: 1,
   borderTopColor: "#f8faf8",
   borderTopWidth: 0.6,
   flexDirection: "row",
   paddingVertical: 20,
   paddingHorizontal: 20
 },menutext:{
     fontFamily:AppSettings.primaryfont,
     fontSize:AppSettings.ternaryFontSize,
     color:AppSettings.white,
     fontWeight:"700",
     paddingLeft:20
 },name:{
    fontFamily:AppSettings.primaryfont,
    fontSize:AppSettings.secondaryFontSize,
    color:AppSettings.white,
    fontWeight:"bold",
    paddingTop:20
 }
});
export default MenuDrawer;