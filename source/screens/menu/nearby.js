import React from "react";
import { StyleSheet, View, Text, Image, Dimensions,TouchableOpacity ,SafeAreaView,FlatList} from "react-native";

import AppSettings from "../../utils/settings"

import MapView, { Polyline, Marker } from "react-native-maps";
import Button from "../../components/button";
import Toolbar from "../../components/toolbar";
import { Navigation } from "react-native-navigation";


const { width,height } = Dimensions.get("window");
class Nearby extends React.Component {
 constructor() {
   super();
   this.state = {
    region: {
        latitude: 27.85,
        longitude:  85.324,
        latitudeDelta:0.005,
        longitudeDelta: 0.005
      },
      Auditions:[
        {location:"Kathmandu",date:"2019 5 august"}, {location:"Pokhara",date:"2019 16 august"}

      ]
 }
 }
 static get options() {
  return {
    topBar: {
      visible: false,
      drawBehind: true
    }
  };
}
register=()=>{
    Navigation.push("App",{
        component:{
            name:"register"
        }
    })
}
 render() {
   return (
     <SafeAreaView style={styles.main}>
     <Toolbar componentId={this.props.componentId} title={"Nearby Auditions"}/>
     <View style={styles.map}>
     <MapView
  initialRegion={this.state.region}
  showsUserLocation
  style={styles.maps}
     
     >
      <Marker
                coordinate={{
                    latitude: 27.85,
                    longitude:  85.324,
                    latitudeDelta:0.005,
                    longitudeDelta: 0.005
                }}
              ></Marker>
              </MapView>
     </View>
     <View style={styles.descriptions}>

     <Text style={styles.Text}>
     Nearest Auditions :
     </Text>
     <Text style={styles.Text}>
     Date :
     </Text>
     </View>
     <FlatList 
     showsVerticalScrollIndicator={false}
     data={this.state.Auditions}
     renderItem={({item})=>{
       return(
         <View style= {[styles.descriptions]}>
         <Text style={styles.Text}>{item.location}</Text>
         <Text style={styles.Text}>{item.date}</Text>
         </View>
       )
     }}
     keyExtractor={(item)=>item.location}
     />
     <TouchableOpacity style={styles.bottombutton} onPress={()=>this.register()}>
     <Button title={"Register Now"} backgroundColor={AppSettings.secondaryColor} borderColor={AppSettings.secondaryColor}/>
      </TouchableOpacity>
     </SafeAreaView>
   );
 }
}
const styles = StyleSheet.create({
 main: {
   backgroundColor: AppSettings.primaryColor,
   flex: 1,
   paddingBottom: 30
 },
 bottombutton:{
    flex:1,position:"absolute",bottom:60,right:20,left:20
  },
maps:{
    width:"100%",
    height:height*0.33,marginVertical:30
},
Text:{
    fontSize: AppSettings.secondaryFontSize,
    fontFamily: AppSettings.primaryFont,
    color: AppSettings.white,paddingTop:5
},descriptions:{
    marginHorizontal:30 ,
    flexDirection:"row",
    justifyContent:"space-between",

}
});
export default Nearby;