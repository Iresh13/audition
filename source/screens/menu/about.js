import React from "react"
import {
    Text,
    View,
    StyleSheet,
    TouchableHighlight,
    Alert,
    SafeAreaView,
    ScrollView,
    AsyncStorage,
    StatusBar
  } from "react-native";
  import { Navigation } from "react-native-navigation";
import Toolbar from "../../components/toolbar";
import AppSettings from "../../utils/settings";

  
  class About extends React.Component {
      constructor(){
          super()
         
      }
    static options() {
        return {
          topBar: {
            visible: false,
            drawBehind: true
          }
        };
      }
      
    render() {
      return (
        <SafeAreaView style={{ flex: 1,backgroundColor:AppSettings.primaryColor }}>
        <Toolbar title={"About Us"} componentId={this.props.componentId}/>
    <ScrollView style={styles.main}>
    <Text style={styles.text}>
    Local image added natively doesn't load images for me, local image in the JS project get loaded.
Current workaround is to load it as an Image from react-native (as it seems that for this case there is no difference)
    </Text>
    
    
    </ScrollView>
          
        </SafeAreaView>
      );
    }
  }
  
  const styles=StyleSheet.create({
main:{
    marginHorizontal:30,
    marginVertical:30,

},
text:{
    fontSize:AppSettings.ternaryFontSize,
    color:AppSettings.white,fontFamily:AppSettings.primaryfont
}


  })
  export default About;