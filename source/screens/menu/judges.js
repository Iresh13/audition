import React from "react"
import {View,TouchableOpacity,Text,Image,SafeAreaView,FlatList,StyleSheet} from "react-native"
import Toolbar from "../../components/toolbar";
import Icon from "react-native-vector-icons/Entypo"
import AppSettings from "../../utils/settings";
import FastImage from "react-native-fast-image";
class Judges extends React.Component{
constructor(){
    super()
    this.state={
        judges:
           [{name:"Hari Hello",
           title:"Singer",
           image:"https://unsplash.it/400/400?image=1"},
           {name:"Ram hello",title:"Dancer",image:"https://unsplash.it/400/400?image=1",
        },
        {name:"Kbc",
        title:"Actor",
        image:"https://unsplash.it/400/400?image=1"}
        ]    
    }
}
static get options(){
    return {
        topBar: {
          visible: false,
          drawBehind: true
        }
      };
}

render(){
    return(
        <SafeAreaView style={styles.main}>
        <Toolbar title={"Judges"} componentId={this.props.componentId}/>
        <View style={{flex:1,margin:30}}>
        <FlatList
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}
        data={this.state.judges}
        renderItem={({item})=>{
            return(
<View style={styles.box}>
<View style ={styles.top}>
<FastImage source={{uri:item.image}} style={styles.image}/>
<Text style={styles.Text}>{item.name}</Text>
<Text style={[styles.Text,{fontSize:AppSettings.ternaryFontSize,fontWeight:"300",paddingTop:5}]}>{item.title}</Text>
</View>
<View style={styles.bottom}>
<TouchableOpacity style={styles.button}>
<Icon name={"instagram-with-circle"} size={40} color={"#e95950"}/>
</TouchableOpacity>
<TouchableOpacity style={styles.button}>
<Icon name={"facebook-with-circle"} size={40} color={"#4267B2"}/>
</TouchableOpacity>
<TouchableOpacity style={styles.button}>
<Icon name={"twitter-with-circle"} size={40} color={"#38A1F3"}/>
</TouchableOpacity>
</View>


</View>






            )
        }}
        
keyExtractor={(item)=>item.id}
        />
        </View>
        
        </SafeAreaView>
    )
}
}
const styles=StyleSheet.create({
main:{
flex:1,
backgroundColor:AppSettings.primaryColor
},
box:{
height:300,
backgroundColor:AppSettings.white,
elevation:5,shadowOpacity:0.2,shadowColor:AppSettings.black,
borderRadius:10,
padding:10,marginTop:10

},
Text:{
fontFamily:AppSettings.primaryfont,
paddingTop:20,
fontSize:AppSettings.secondaryFontSize,
color:AppSettings.primaryColor,
fontWeight:"500"
},
bottom:{
    flexDirection:"row",
    justifyContent:"space-between",
    alignItems:"center",
    paddingTop:20,
    paddingHorizontal:60
},
button:{
    alignItems:"center",
    justifyContent:"center",
    height:40,width:40,
    borderRadius:20,
    backgroundColor:AppSettings.white,
    elevation:3,shadowOpacity:0.1,
    shadowColor:AppSettings.black
},
image:{
height:140,
width:"100%",
borderRadius:5,
marginTop:10
},
top:{
    justifyContent:"center",
    alignItems:"center"
}
})

export default Judges;