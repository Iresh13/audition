import React from "react";
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  Modal,
  TouchableOpacity,
  Dimensions,
  SafeAreaView,
 
} from "react-native";

import StarRating from "react-native-star-rating";
import AppSettings from "../../utils/settings";
import { Navigation } from "react-native-navigation";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scrollview";
import Toolbar from "../../components/toolbar";
import Button from "../../components/button";




class Feedback extends React.Component {
  constructor() {
    super();
    this.state = {
      star: 0,
      comments: "",
   
    };
  }


  

  static get options() {
    return {
      topBar: {
        visible: false,
        drawBehind: true
      }
    };
  }
  
 
    starpress = rating => {
        this.setState({ star: rating })
      };

  render() {
    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: AppSettings.primaryColor}}>
        <Toolbar componentId={this.props.componentId} title={"FeedBack"}/>

        <KeyboardAwareScrollView style={styles.main}>
     
          <View style={styles.rating}>
            <Text style={[styles.text, { color: AppSettings.white }]}>
           Rate us
            </Text>
            <View style={styles.Star}>
              <StarRating
                disabled={false}
                maxStars={5}
                rating={this.state.star}
                fullStarColor={"yellow"}
                starSize={15}
                starStyle={{ paddingRight: 5 }}
                halfStarEnabled={true}
                selectedStar={rating => this.starpress(rating)}
              />
            </View>
          </View>

          <View style={styles.box}>
            <TextInput
              placeholder={"Comments"}
              placeholderTextColor={AppSettings.white}
              onChangeText={text => this.setState({ comments: text })}
              style={styles.TextInput}
              multiline
            />
          </View>

        
         
        </KeyboardAwareScrollView>
        <TouchableOpacity style={styles.bottombutton}>
     <Button title={"Rate Us"} backgroundColor={AppSettings.secondaryColor} borderColor={AppSettings.secondaryColor}/>
      </TouchableOpacity>
      </SafeAreaView>
    );
  }
}
const { height, width } = Dimensions.get("window");
const styles = StyleSheet.create({
  main: {
    flex: 1,
    marginHorizontal: 30,
    backgroundColor: AppSettings.primaryColor   
  },

  rating: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingTop: 20
  },
  box: {
    height: 100,
    borderColor: AppSettings.white,
    borderWidth: 0.3,
    marginTop: 30,
    borderRadius: 5,
    padding:10 
  },
  TextInput: {
    fontSize: AppSettings.mediumFontSize,
    fontFamily: AppSettings.primaryFont,
    color: AppSettings.white,
    paddingLeft: 10,
    paddingTop: 5
  },
  button: {
    paddingTop: 30,
    paddingBottom: 20,
    flex: 1
  },
  Star: { width: 100 },
  text: {
    fontSize: AppSettings.secondaryFontSize,
    fontFamily: AppSettings.primaryFont,
    color: AppSettings.white
  },
  bottombutton:{
    flex:1,position:"absolute",bottom:60,right:20,left:20
  },
});

export default Feedback;