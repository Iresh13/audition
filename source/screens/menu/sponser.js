import React from "react"
import {
    Text,
    View,
    StyleSheet,
    TouchableHighlight,
    Alert,
    SafeAreaView,
    ScrollView,FlatList,Image,
    AsyncStorage,
    StatusBar
  } from "react-native";
  import { Navigation } from "react-native-navigation";
import Toolbar from "../../components/toolbar";
import AppSettings from "../../utils/settings";

  
  class Sponsers extends React.Component {
      constructor(){
          super()
         this.state={
             mainsponsers:[
                 {
                     title:"name",image:require("../../assets/coke.png")
                 },
                 {
                    title:"name",image:require("../../assets/coke.png")
                },
              
             ],
             sponsers:[
             {
                title:"name",image:require("../../assets/coke.png")
            },
            {
                title:"name",image:require("../../assets/coke.png")
           },
           {
            title:"name",image:require("../../assets/coke.png")
       }
        ],
         }
      }
    static options() {
        return {
          topBar: {
            visible: false,
            drawBehind: true
          }
        };
      }
      
    render() {
      return (
        <SafeAreaView style={{ flex: 1,backgroundColor:AppSettings.primaryColor }}>
        <Toolbar title={"Sponsers"} componentId={this.props.componentId}/>
    <View style={styles.main}>
    <View style={{justifyContent:"center",alignItems:"center"}}>
    <Text style={[styles.name,{paddingBottom:10,fontSize:AppSettings.primaryFontSize}]}>Main Partners</Text>
    </View>
  <FlatList
  data={this.state.mainsponsers}
  showsVerticalScrollIndicator={false}
renderItem={({item})=>{
    return(
<View style={styles.mains}>
<View style={styles.circle}>
<Image style={styles.mainimage} source={item.image}>
</Image>
</View>
<Text style={styles.name}>{item.title}</Text>
</View>
    )
}}
keyExtractor={(item)=>item.title}
  />
  <View style={{justifyContent:"center",alignItems:"center"}}>
<Text style={[styles.name,{paddingBottom:10,fontSize:AppSettings.secondaryFontSize}]}>Partners</Text>
</View>
<FlatList
  data={this.state.sponsers}
  horizontal
  showsHorizontalScrollIndicator={false}
renderItem={({item})=>{
    return(
<View style={styles.sub}>
<View style={styles.smallcircle}>
<Image style={styles.subimage}  source={item.image}>
</Image>
</View>
<Text style={styles.name}>{item.title}</Text>
</View>
    )
}}
keyExtractor={(item)=>item.title}
  />

    
    
    </View>
          
        </SafeAreaView>
      );
    }
  }
  
  const styles=StyleSheet.create({
main:{
    marginHorizontal:30,
    marginVertical:30,

},
name:{
    fontSize:AppSettings.ternaryFontSize,
    color:AppSettings.white,
    fontFamily:AppSettings.primaryfont,
    
},
subimage:{
    height:80,
    width:80,
    borderRadius:40
},smallcircle:{
    height:82,
    width:82,
    borderRadius:41,
    backgroundColor:AppSettings.white,
    justifyContent:"center",
    alignItems:"center"
},
mainimage:{
    height:100,
    width:100,
    borderRadius:50
},circle:{
    height:102,
    width:102,
    borderRadius:51,
    backgroundColor:AppSettings.white,
    justifyContent:"center",
    alignItems:"center"
},
mains:{
    justifyContent:"center",alignItems:"center",marginVertical:20
},
sub:{
    justifyContent:"center",
    alignItems:"center",
    marginHorizontal:20,
    marginVertical:20
}


  })
  export default Sponsers;