import React from "react";
import { StyleSheet, View, Text, Image, Dimensions,TouchableOpacity,Animated,Easing,ScrollView } from "react-native";
import Button from "../components/button"
import Carousel, { Pagination } from "react-native-snap-carousel";
import AppSettings from "../utils/settings"
import Icon from "react-native-vector-icons/AntDesign"
import { Navigation } from "react-native-navigation";
import FastImage from "react-native-fast-image";



const { width,height } = Dimensions.get("window");
class Home extends React.Component {
 constructor() {
   super();
   this.state = {
     vehices: [

       {  image: require("../assets/user.png"),text:"hello"},
       {
         
        image: require("../assets/Unknown.png"),text:"hello"},
       
       {
       image: require("../assets/coke.png"),text:"hello"}
     ]
   };
   this.height = new Animated.Value(-60)
 }

 static get options() {
  return {
    topBar: {
      visible: false,
      drawBehind: true
    }
  };
}
componentDidMount=()=>{
Animated.timing(this.height,{
  toValue:30,
  duration:1000,
  easing:Easing.linear

}).start()
}
gotoRegister=()=>{
  Navigation.push("App",{
    component:{
      name:"register"
    }
  })
}
showMenu=()=>{
Navigation
.mergeOptions("Drawer", {
           sideMenu: {
             left: {
               visible: true,
               enabled: true
             }
           }
         })
}
 render() {
   return (
     <ScrollView style={styles.main}>
     <Animated.View style={[styles.menu,{top:this.height}]} >
     <TouchableOpacity style={{flex:1,justifyContent:"center",alignItems:"center"}} onPress={()=>this.showMenu()}>
     <Icon name={"menuunfold"}size={20}color={AppSettings.primaryColor}/>
     </TouchableOpacity>
     </Animated.View>

     <View style={styles.logo}>
     <FastImage source={require("../assets/logo.png")} style={{height:90,width:90}}/>
     </View>
     <View style={styles.Carousel}>
       <Carousel
         data={this.state.vehices}
         style={[styles.modalview]}
         sliderWidth={width}
         firstItem={1}
         itemWidth={width / 2} 
         autoplayDelay={1500}
         autoplayInterval={3500}
         renderItem={({ item, index }) => {
           return (
             <View style={styles.card}>
         <FastImage source={item.image} style={styles.images}/>
         <Text style={styles.Text}>{item.text}</Text>
             </View>
           );
         }}
       />
       </View>

       <TouchableOpacity style={styles.button} onPress={() => this.gotoRegister()}>
         <Button
           title={"Register Now"}
           backgroundColor={AppSettings.secondaryColor}
             borderColor={AppSettings.secondaryColor}
         />
       </TouchableOpacity>
       <View style={{flex:2,justifyContent:"center"}}>
       <View style={styles.cardlayout}>
       <Text style={styles.Text}>This is news section.</Text>
     
    </View>
    <View style={styles.cardlayout}>
       <Text style={styles.Text}>This is news section.</Text>
     
    </View>
       </View>
     </ScrollView>
   );
 }
}
const styles = StyleSheet.create({
 main: {
   backgroundColor: AppSettings.primaryColor,
   flex: 1,
  
 },
 card: {
   height: 200,
   width: 200,
   borderRadius: 10,
   borderColor: "white",
   borderWidth: 0.3,
   elevation: 5,
   shadowColor: "black",
   shadowOpacity: 0.2,
   alignItems:"center",
   backgroundColor: "white",paddingTop:10
 },
 images:{
  height: 150,
  width: 198,
  borderRadius: 10
 },
 button: {
   paddingVertical: 10,
   height: 60,paddingHorizontal:30,marginVertical:30
 },
 modalview: {
   height: 170,
 },
 Carousel:{
  marginTop:height*0.25,
   flex:2
 },
 Text:{
  fontFamily:AppSettings.primaryfont,
  paddingTop:20,
  fontSize:AppSettings.ternaryFontSize,
  color:AppSettings.primaryColor,
  fontWeight:"300"
  },
 menu:{
   position:"absolute",
   left:20,
   top:15,
   height:50,
   width:50,
   borderRadius:25,
   backgroundColor:AppSettings.white,
   elevation:5,
   shadowOpacity:0.2,
   shadowColor:AppSettings.black,justifyContent:"center",alignItems:"center"
 },logo:{
  position:"absolute",
  top:25,
  right:width*0.5-45
 },cardlayout:{
  borderRadius: 20,
  borderColor: "white",
  borderWidth: 0.3,
  elevation: 5,
  shadowColor: "black",
  shadowOpacity: 0.2,
  alignItems:"center",
  backgroundColor: "white",
  height:100, 
  marginHorizontal:30,marginVertical:10,alignItems:"center"
 }
});
export default Home;