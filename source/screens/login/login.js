import React from 'react';
import {
  Text,
  View,
  StyleSheet,
  Dimensions,
  SafeAreaView,
  TouchableOpacity,
  AsyncStorage,
} from 'react-native';
import {Navigation} from 'react-native-navigation';
import AppSettings from '../../utils/settings';
import Icon from 'react-native-vector-icons/AntDesign';
import FastImage from 'react-native-fast-image';
import {LoginManager} from 'react-native-fbsdk';
import {GoogleSignin, statusCodes} from 'react-native-google-signin';

class Login extends React.Component {
  async componentDidMount() {
    GoogleSignin.configure({
      scopes: ['https://www.googleapis.com/auth/drive.readonly'],
      webClientId: 'AIzaSyCeHbm347HKVIiNbaUMt76eyQH8T9Fdafw',
    });
  }
  static get options() {
    return {
      topBar: {
        visible: false,
        drawBehind: true,
      },
    };
  }

  logFb = async () => {
    LoginManager.logInWithPermissions(['public_profile']).then(
      function(result) {
        if (result.isCancelled) {
          console.log('Login cancelled');
        } else {
          console.log(
            'Login success with permissions: ' +
              result.grantedPermissions.toString(),
          );
        }
      },
      function(error) {
        console.log('Login fail with error: ' + error);
      },
    );
  };

  skip=()=>{
    Navigation.setRoot({
      root: {
        sideMenu: {
          id: "sideMenu",
          left: {
            stack: {
              children: [
                {
                  component: {
                    id: "Drawer",
                    name: "menu"
                  }
                }
              ]
            }
          },
          center: {
            stack: {
              id: "AppRoot",
              children: [
                {
                  component: {
                    id: "App",
                    name: "home"
                  }
                }
              ]
            }
          }
        }
      }
    });
  }

  logGoogle = async () => {
    try {
      const userInfo = await GoogleSignin.signIn();
      console.log('User Info --> ', userInfo);
    } catch (error) {
      console.log('Message', error.message);
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        console.log('User Cancelled the Login Flow');
      } else if (error.code === statusCodes.IN_PROGRESS) {
        console.log('Signing In');
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        console.log('Play Services Not Available or Outdated');
      } else {
        console.log('Some Other Error Happened');
      }
    }
  };

  render() {
    return (
      <SafeAreaView style={styles.main}>
        <View style={styles.image}>
          <FastImage
            source={require('../../assets/logo.png')}
            style={{height: 120, width: 120}}
          />
        </View>
        <View style={{flex: 1}}>
          <TouchableOpacity style={styles.button} onPress={() => this.logFb()}>
            <Icon
              name={'facebook-square'}
              size={30}
              color={'#4267B2'}
              style={{paddingLeft: 20}}
            />
            <Text style={styles.buttontext}>Login With Facebook</Text>
            <View />
          </TouchableOpacity>

          <TouchableOpacity
            style={styles.button}
            onPress={() => this.logGoogle()}>
            <Icon
              name={'google'}
              size={30}
              color={'#db3236'}
              style={{paddingLeft: 20}}
            />
            <Text style={styles.buttontext}>Login With Google</Text>
            <View />
          </TouchableOpacity>
        </View>
        <TouchableOpacity style={{position:"absolute",bottom:20,right:20}} onPress={()=>this.skip()}>
         <Text style={{color:AppSettings.white}}>Skip</Text>
          </TouchableOpacity>
      </SafeAreaView>
    );
  }
}
const {height, width} = Dimensions.get('screen');
const styles = StyleSheet.create({
  main: {
    flex: 1,
    backgroundColor: AppSettings.primaryColor,
    padding: 30,
    justifyContent: 'center',
  },
  image: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',
  },
  button: {
    height: 50,
    paddingVertical: 10,
    justifyContent: 'space-between',
    alignItems: 'center',
    borderRadius: 10,
    elevation: 5,
    shadowOpacity: 0.1,
    shadowColor: AppSettings.black,
    flexDirection: 'row',
    backgroundColor: AppSettings.white,
    borderColor: AppSettings.white,
    marginVertical: 10,
    marginHorizontal: 30,
  },
  buttontext: {
    fontSize: AppSettings.ternaryFontSize,
    fontFamily: AppSettings.primaryfont,
    color: AppSettings.black,
    fontWeight: '700',
  },
});
export default Login;
