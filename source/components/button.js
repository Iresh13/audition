import React from "react"
import {View,Text,StyleSheet} from "react-native"
import AppSettings from "../utils/settings";

class Button extends React.Component{
    constructor(props){
        super(props)
    }
render(){
    return(
        <View style={[styles.Button,{backgroundColor:
        this.props.backgroundColor,borderColor:this.props.borderColor}]}>
        <Text style={styles.title}>{this.props.title}</Text>
        
        </View>
    )
}


}
const styles=StyleSheet.create
({
    Button:{
       height:50,
        paddingVertical:10,
        justifyContent:"center",
        alignItems:"center",borderRadius:10,
        elevation:5,shadowOpacity:0.1,shadowColor:AppSettings.black
    },
    title:{
        fontSize:AppSettings.ternaryFontSize,fontFamily:AppSettings.primaryfont,color:AppSettings.white,fontWeight:"700"
    }
})
export default Button;