import React from "react"
import {View,Text,StyleSheet,TouchableOpacity} from "react-native"
import AppSettings from "../utils/settings";
import Icon from "react-native-vector-icons/Ionicons"
import {Navigation} from "react-native-navigation"
class Toolbar extends React.Component{
    goBack = () => {
        Navigation.pop(this.props.componentId);
      };
render(){
    return(
        <View style={styles.Toolbar}>
        <TouchableOpacity style={styles.back} onPress={()=>this.goBack()}>
        <Icon name={"ios-arrow-round-back"}
                size={40}
                color={AppSettings.white}/>
        
        </TouchableOpacity>
        <Text style={styles.title}>{this.props.title}</Text>
        </View>
    )
}


}
const styles=StyleSheet.create
({
    Toolbar:{
       height:60,
       paddingHorizontal:10,
        paddingVertical:10,
        flexDirection:"row",
        alignItems:"center",
        backgroundColor:AppSettings.secondaryColor,
        elevation:5,shadowOpacity:0.1,shadowColor:AppSettings.black
    },
    title:{
        fontSize:AppSettings.secondaryFontSize,
        fontFamily:AppSettings.primaryfont,
        color:AppSettings.white,fontWeight:"700",paddingLeft:90
    },
    back:{
        height:50,
        width:50
        ,borderRadius:25,
        backgroundColor:"rgba(0,0,0,0)",
        justifyContent:"center",
        alignItems:"center"
    }
})
export default Toolbar;