import { Navigation} from "react-native-navigation"
import Home from "../screens/home";
import Login from "../screens/login/login";
import About from "../screens/menu/about";
import Feedback from "../screens/menu/feedback";
import Judges from "../screens/menu/judges";
import Sponsers from "../screens/menu/sponser";
import MenuDrawer from "../screens/menu";
import Register from "../screens/register";
import Nearby from "../screens/menu/nearby";
import App from "../../App";


export function RegisterScreen(){
    Navigation.registerComponent(`App`, () => App);
    Navigation.registerComponent("home",()=>Home),
    Navigation.registerComponent("login",()=>Login),
    Navigation.registerComponent("sponser",()=>Sponsers),
    Navigation.registerComponent("about",()=>About),
    Navigation.registerComponent("feedback",()=>Feedback),
    Navigation.registerComponent("judges",()=>Judges),
    Navigation.registerComponent("menu",()=>MenuDrawer), 
    Navigation.registerComponent("register",()=>Register)
    Navigation.registerComponent("nearby",()=>Nearby)

}