const AppSettings={
primaryfont:"Arial",
primaryColor: "rgb(32,32,43)",
secondaryColor: "rgb(25,25,32)",
primaryFontSize: 20,
secondaryFontSize: 18,
ternaryFontSize: 16,
smallFontSize: 14,
xsmallFontSize: 12,
white: "#FFFFFF",
black: "#000000",

}
export default AppSettings