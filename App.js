import React from "react"
import {
  Text,
  View,
  StyleSheet,
  TouchableHighlight,
  Alert,
  SafeAreaView,
  ScrollView,
  AsyncStorage,
  StatusBar
} from "react-native";
import { Navigation } from "react-native-navigation";
import AppSettings from "./source/utils/settings";

class App extends React.Component {

componentDidMount=async()=> {
   
  
    Navigation.setRoot({
      root: {
        stack: {
          id: "AppRoot",
          children: [
            {
              component: {
                id: "main",
                name: "login"
              }
            }
          ]
        }
      }
    });
   
  
};
  render() {
    return (
      <SafeAreaView style={{ flex: 1,
      backgroundColor:AppSettings.secondaryColor,
      justifyContent:"center",
      alignItems:"center" }}>
      </SafeAreaView>
    );
  }
}

export default App;